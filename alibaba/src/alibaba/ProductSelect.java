package alibaba;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ProductSelect {


	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		String contact = null;

		//	driver.get("http://www.alibaba.com/trade/search?fsb=y&IndexArea=product_en&CatId=&SearchText=electronic");

		//login to alibaba
		driver.get("https://login.alibaba.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
		//Switching to  iFrame
		driver.switchTo().frame("alibaba-login-box");

		//Entering login credentials
		driver.findElement(By.id("fm-login-id")).sendKeys("agravaris@lasoft.org");
		driver.findElement(By.id("fm-login-password")).sendKeys("nlsz987y124rfjBKL");
		//Pressing the submit button
		driver.findElement(By.id("fm-login-submit")).click();


		//		 setting the search criteria
		driver.findElement(By.xpath(".//*[@id='home-searchbar']/div/form/div[2]/input")).sendKeys("electronic");
		driver.findElement(By.xpath(".//*[@id='home-searchbar']/div/form/input[4]")).click();


		//clicking the location dropdown
		driver.findElement(By.id("J-m-location-trigger")).click();

		//Selecting the country
		driver.findElement(By.xpath("(//span[text()='South Korea'])[2]")).click();

		List<WebElement> products = driver.findElements(By.xpath("//a[contains(@class,'csp')]"));
		//selecting a products contact
		//		driver.findElement(By.xpath(".//*[@id='J-m-product-items']/div[1]/div[2]/div[2]/div/a[1]")).click();
		String parentWindow = driver.getWindowHandle();
		for (int i = 2; i <= 7; i++){
			products.get(i).click();

			// Switching from parent window to child window   
			Set<String> windows = driver.getWindowHandles();
			windows.remove(parentWindow);

			for (String ChildWindow : windows) {  
				driver.switchTo().window(ChildWindow);
				try {
					// Performing actions on child window  

					// Locating the name of the costumer
					contact = driver.findElement(By.xpath("//span[contains(@class,'company-contact')]")).getText();
				} catch(WebDriverException e){
					System.out.println("Wrong window");
				}
				contact = contact.substring(0, contact.indexOf(' ')+2) + '.';
				System.out.println(contact);


				//				Writing the content of the email
				driver.switchTo().frame("inquiry-content_ifr");
				driver.findElement(By.className("mce-content-body")).sendKeys(
						"Hello MR." +contact +"\nThis is test message, please do not reply");  

				driver.switchTo().parentFrame();    // to move back to parent frame

				// Pressing the "Send" button
				driver.findElement(By.xpath("(//input[contains(@class,'button')])[1]")).click();
			}
			driver.close();
			//Switching back to Parent Window  
			driver.switchTo().window(parentWindow);  
		}
		//Performing some actions on Parent Window  
		//   driver.findElement(By.className("btn_style")).click(); 


		driver.quit();
	}

}

